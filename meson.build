project('banking',
          version: '0.6.0',
    meson_version: '>= 0.51.0',
  default_options: [ 'warning_level=2',
                   ],
)

# Importing modules
gnome = import('gnome')
i18n = import('i18n')
python = import('python')

# Module objects
py_installation = python.find_installation('python3', modules: ['fints', 'schwifty', 'onetimepad', 'cryptography', 'gi.repository'])

# Make sure Python is installed and found
if not py_installation.found()
    error('No valid python3 binary found')
endif

# Constants
PACKAGE_URL = 'https://www.tabos.org/projects/banking/'
PACKAGE_URL_BUG = 'https://www.gitlab.com/tabos/banking/issues'
PROJECT_RDNN_NAME = 'org.tabos.banking'

APPLICATION_ID = 'org.tabos.banking'
PYTHON_DIR = py_installation.get_path('purelib')
PKGDATA_DIR = join_paths(get_option('prefix'), get_option('datadir'), APPLICATION_ID)
PKGLIB_DIR = join_paths(get_option('prefix'), get_option('libdir'), APPLICATION_ID)

# Dependencies
dependency('libadwaita-1', version: '>= 1.2.0')
xmllint = find_program('xmllint')

subdir('banking')
subdir('data/ui')
subdir('data')
subdir('po')

install_subdir(
    'banking',
    install_dir: py_installation.get_install_dir()
)

# Install the executable file
conf = configuration_data()
conf.set('application_id', APPLICATION_ID)
conf.set('rdnn_name', PROJECT_RDNN_NAME)
conf.set('pkgdatadir', PKGDATA_DIR)
conf.set('localedir', join_paths(get_option('prefix'), get_option('datadir'), 'locale'))
conf.set('pythondir', PYTHON_DIR)
conf.set('schemasdir', PKGDATA_DIR)

conf.set('local_build', 'False')

configure_file(
  input: 'banking.in',
  output: 'org.tabos.banking',
  configuration: conf,
  install_dir: get_option('bindir')
)

# Install builddir executable
local_conf = configuration_data()
local_conf.set('application_id', APPLICATION_ID)
local_conf.set('rdnn_name', PROJECT_RDNN_NAME)
local_conf.set('pkgdatadir', join_paths(meson.build_root(), 'data'))
local_conf.set('localedir', join_paths(get_option('prefix'), get_option('datadir'), 'locale'))
local_conf.set('pythondir',  meson.source_root())
local_conf.set('schemasdir', join_paths(meson.build_root(), 'data'))

local_conf.set('local_build', 'True')

configure_file(
  input: 'banking.in',
  output: 'local-org.tabos.banking',
  configuration: local_conf,
)

meson.add_postconf_script('meson_post_conf.py')
meson.add_install_script('meson_post_install.py')

