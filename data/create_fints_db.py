import json
import openpyxl

data = {}
data['databases'] = []

BLZ = 1
BIC = 2
INSTITUTE = 3
CITY = 4
ORGANIZATION = 6
URL = 24

def get_logo(bank: str) -> str:
    if bank == 'dsgv':
        return 'dsgv'
    if bank == 'bvr':
        return 'bvr'

    return 'bank'

# Open official list
wb = openpyxl.load_workbook("fints_institute.xlsx", data_only=True)
worksheet = wb["fints_institute_Master"]
for row in worksheet.iter_rows(min_row=2):
    if row[BLZ].value:
        data['databases'].append({
            'blz': row[BLZ].value,
            'bic': str(row[BIC].value).lower(),
            'institute': row[INSTITUTE].value,
            'logo': get_logo(row[ORGANIZATION].value.lower() if row[ORGANIZATION].value else ''),
            'url': row[URL].value,
            'city': row[CITY].value
        })

# Add custom entries
data['databases'].append({
        'blz': "20041177",
        'bic': '',
        'institute': 'comdirect bank AG',
        'logo': 'bank',
        'url': 'https://fints.comdirect.de/fints',
        'city': 'Unknown'
    })

# Demo data for testing purpose
data['databases'].append({
        'blz': "00000000",
        'bic': '',
        'institute': 'Demo Bank',
        'logo': 'bank',
        'url': 'http://0.0.0.0',
        'city': 'Virtual'
    })

with open('resources/database.json', 'w') as outfile:
    json.dump(data, outfile)
